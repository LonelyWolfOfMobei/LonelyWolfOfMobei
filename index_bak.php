<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>>_JeeZhan/主页</title>

    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="Bookmark" href="images/favicon.ico">
    <!-- 百度统计 -->
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?ede7cc6187020c04f6ae0f9086393c7c";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <!-- 静态星空背景 -->
    <style>
        body {
            width: 100%;
            height: 1000px;
            background: linear-gradient(to bottom right, #000, #2073c2 60%, #dff6ff);
            overflow: hidden;
        }
        span {
            display: block;
            position: absolute;
            border-radius: 50%;
            box-shadow: 0.4px 0.4px 0.4px 0px #fff;
        }
    </style>
</head>

<body style="margin: 0px;">
    <!-- 静态星空背景 -->
    <script>
        window.onload = function () {
            var screenW = document.documentElement.clientWidth;
            var screenH = document.documentElement.clientHeight;
            var colorArr = ['#fff', 'skyblue', 'orange'];
            console.log(screenW);
            for (var i = 0; i < 800; i++) {
                var span = document.createElement('span');
                var width = Math.random() * 3;
                var colorIndex = parseInt(Math.random() * 3);
                var x = parseInt(Math.random() * screenW);
                var y = parseInt(Math.random() * screenH);
                span.style.width = parseInt(width) + 'px';
                span.style.height = parseInt(width) + 'px';
                span.style.background = colorArr[colorIndex];
                span.style.left = x + 'px';
                span.style.top = y + 'px';

                document.body.appendChild(span);
            }
        }
    </script>
    <!-- 鼠标移动星星拖尾 -->
    <span class="js-cursor-container"></span>
    <script>
        (function fairyDustCursor() {
            var possibleColors = ["#D61C59", "#E7D84B", "#1B8798"]
            var width = window.innerWidth;
            var height = window.innerHeight;
            var cursor = { x: width / 2, y: width / 2 };
            var particles = [];
            function init() {
                bindEvents();
                loop();
            }
            // Bind events that are needed
            function bindEvents() {
                document.addEventListener('mousemove', onMouseMove);
                window.addEventListener('resize', onWindowResize);
            }
            function onWindowResize(e) {
                width = window.innerWidth;
                height = window.innerHeight;
            }
            function onMouseMove(e) {
                cursor.x = e.clientX;
                cursor.y = e.clientY;
                addParticle(cursor.x, cursor.y, possibleColors[Math.floor(Math.random() * possibleColors.length)]);
            }
            function addParticle(x, y, color) {
                var particle = new Particle();
                particle.init(x, y, color);
                particles.push(particle);
            }
            function updateParticles() {
                // Updated
                for (var i = 0; i < particles.length; i++) {
                    particles[i].update();
                }
                // Remove dead particles
                for (var i = particles.length - 1; i >= 0; i--) {
                    if (particles[i].lifeSpan < 0) {
                        particles[i].die();
                        particles.splice(i, 1);
                    }
                }
            }
            function loop() {
                requestAnimationFrame(loop);
                updateParticles();
            }
            /**
             * Particles
             */
            function Particle() {
                this.character = "*";
                this.lifeSpan = 120; //ms
                this.initialStyles = {
                    "position": "fixed",
                    "display": "inline-block",
                    "top": "0px",
                    "left": "0px",
                    "pointerEvents": "none",
                    "touch-action": "none",
                    "z-index": "10000000",
                    "fontSize": "25px",
                    "will-change": "transform"
                };
                // Init, and set properties
                this.init = function (x, y, color) {
                    this.velocity = {
                        x: (Math.random() < 0.5 ? -1 : 1) * (Math.random() / 2),
                        y: 1
                    };
                    this.position = { x: x + 10, y: y + 10 };
                    this.initialStyles.color = color;
                    this.element = document.createElement('span');
                    this.element.innerHTML = this.character;
                    applyProperties(this.element, this.initialStyles);
                    this.update();
                    document.querySelector('.js-cursor-container').appendChild(this.element);
                };
                this.update = function () {
                    this.position.x += this.velocity.x;
                    this.position.y += this.velocity.y;
                    this.lifeSpan--;
                    this.element.style.transform = "translate3d(" + this.position.x + "px," + this.position.y + "px, 0) scale(" + (this.lifeSpan / 120) + ")";
                }
                this.die = function () {
                    this.element.parentNode.removeChild(this.element);
                }
            }
            /**
             * Utils
             */
            // Applies css `properties` to an element.
            function applyProperties(target, properties) {
                for (var key in properties) {
                    target.style[key] = properties[key];
                }
            }
            if (!('ontouchstart' in window || navigator.msMaxTouchPoints)) init();
        })();
    </script>
    <!-- 网页鼠标点击特效（爱心） -->
    <script type="text/javascript">
        ! function (e, t, a) {
            function r() {
                for (var e = 0; e < s.length; e++) s[e].alpha <= 0 ? (t.body.removeChild(s[e].el), s.splice(e, 1)) : (s[
                    e].y--, s[e].scale += .004, s[e].alpha -= .013, s[e].el.style.cssText = "left:" + s[e].x +
                    "px;top:" + s[e].y + "px;opacity:" + s[e].alpha + ";transform:scale(" + s[e].scale + "," + s[e]
                        .scale + ") rotate(45deg);background:" + s[e].color + ";z-index:99999");
                requestAnimationFrame(r)
            }
            function n() {
                var t = "function" == typeof e.onclick && e.onclick;
                e.onclick = function (e) {
                    t && t(), o(e)
                }
            }
            function o(e) {
                var a = t.createElement("div");
                a.className = "heart", s.push({
                    el: a,
                    x: e.clientX - 5,
                    y: e.clientY - 5,
                    scale: 1,
                    alpha: 1,
                    color: c()
                }), t.body.appendChild(a)
            }
            function i(e) {
                var a = t.createElement("style");
                a.type = "text/css";
                try {
                    a.appendChild(t.createTextNode(e))
                } catch (t) {
                    a.styleSheet.cssText = e
                }
                t.getElementsByTagName("head")[0].appendChild(a)
            }
            function c() {
                return "rgb(" + ~~(255 * Math.random()) + "," + ~~(255 * Math.random()) + "," + ~~(255 * Math
                    .random()) + ")"
            }
            var s = [];
            e.requestAnimationFrame = e.requestAnimationFrame || e.webkitRequestAnimationFrame || e
                .mozRequestAnimationFrame || e.oRequestAnimationFrame || e.msRequestAnimationFrame || function (e) {
                    setTimeout(e, 1e3 / 60)
                }, i(
                    ".heart{width: 10px;height: 10px;position: fixed;background: #f00;transform: rotate(45deg);-webkit-transform: rotate(45deg);-moz-transform: rotate(45deg);}.heart:after,.heart:before{content: '';width: inherit;height: inherit;background: inherit;border-radius: 50%;-webkit-border-radius: 50%;-moz-border-radius: 50%;position: fixed;}.heart:after{top: -5px;}.heart:before{left: -5px;}"
                ), n(), r()
        }(window, document);
    </script>

    <iframe id="bi_iframe" src="https://flowus.cn/jeep_zhang/share/4717753a-3fa1-4f3d-80f1-72615e4b4f1c"
        onload="adjustIframe();" frameborder="0" scrolling="auto">
    </iframe>
    <script>
        function adjustIframe() {
            var ifm = document.getElementById("bi_iframe");
            ifm.height = document.documentElement.clientHeight;
            ifm.width = document.documentElement.clientWidth;
        }
    </script>

    <!-- <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=450 src="https://music.163.com/outchain/player?type=1&id=35909212&auto=1&height=430"></iframe> -->

    <script src='https://gitee.com/BraveHeart-Studio/braveheart-studio/widget_preview' async defer></script>
    <div id="osc-gitee-widget-tag"></div>
    <style>
        .osc_pro_color {
            color: #4183c4 !important;
        }

        .osc_panel_color {
            background-color: #ffffff !important;
        }

        .osc_background_color {
            background-color: #ffffff !important;
        }

        .osc_border_color {
            border-color: #e3e9ed !important;
        }

        .osc_desc_color {
            color: #666666 !important;
        }

        .osc_link_color * {
            color: #9b9b9b !important;
        }
    </style>

    <center><a href="https://flowus.cn/jeep_zhang/share/4717753a-3fa1-4f3d-80f1-72615e4b4f1c"
            target="_blank">>_JeeZhan/主页 手机版</a> | <a href="https://stats.uptimerobot.com/EVZy9SlrlG"
            target="_blank">网站运行状况</a></center>
</body>
    <!-- 蜘蛛网背景特效（此页面不生效） -->
    <script>
        !function () {
            function n(n, e, t) {
                return n.getAttribute(e) || t
            }
            function e(n) {
                return document.getElementsByTagName(n)
            }
            function t() {
                var t = e("script"), o = t.length, i = t[o - 1];
                return { l: o, z: n(i, "zIndex", -1), o: n(i, "opacity", .5), c: n(i, "color", "0,0,0"), n: n(i, "count", 99) }
            }
            function o() {
                a = m.width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth, c = m.height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
            }
            function i() {
                r.clearRect(0, 0, a, c);
                var n, e, t, o, m, l;
                s.forEach(function (i, x) {
                    for (i.x += i.xa, i.y += i.ya, i.xa *= i.x > a || i.x < 0 ? -1 : 1, i.ya *= i.y > c || i.y < 0 ? -1 : 1, r.fillRect(i.x - .5, i.y - .5, 1, 1), e = x + 1; e < u.length; e++) n = u[e], null !== n.x && null !== n.y && (o = i.x - n.x, m = i.y - n.y, l = o * o + m * m, l < n.max && (n === y && l >= n.max / 2 && (i.x -= .03 * o, i.y -= .03 * m), t = (n.max - l) / n.max, r.beginPath(), r.lineWidth = t / 2, r.strokeStyle = "rgba(" + d.c + "," + (t + .2) + ")", r.moveTo(i.x, i.y), r.lineTo(n.x, n.y), r.stroke()))
                }), x(i)
            }
            var a, c, u, m = document.createElement("canvas"), d = t(), l = "c_n" + d.l, r = m.getContext("2d"),
                x = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (n) {
                    window.setTimeout(n, 1e3 / 45)
                }, w = Math.random, y = { x: null, y: null, max: 2e4 };
            m.id = l, m.style.cssText = "position:fixed;top:0;left:0;z-index:" + d.z + ";opacity:" + d.o, e("body")[0].appendChild(m), o(), window.onresize = o, window.onmousemove = function (n) {
                n = n || window.event, y.x = n.clientX, y.y = n.clientY
            }, window.onmouseout = function () {
                y.x = null, y.y = null
            };
            for (var s = [], f = 0; d.n > f; f++) {
                var h = w() * a, g = w() * c, v = 2 * w() - 1, p = 2 * w() - 1;
                s.push({ x: h, y: g, xa: v, ya: p, max: 6e3 })
            }
            u = s.concat([y]), setTimeout(function () {
                i()
            }, 100)
        }();
    </script>

</html>